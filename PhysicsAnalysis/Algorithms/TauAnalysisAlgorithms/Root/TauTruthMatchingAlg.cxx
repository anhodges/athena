/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack



//
// includes
//

#include <TauAnalysisAlgorithms/TauTruthMatchingAlg.h>

//
// method implementations
//

namespace CP
{

  StatusCode TauTruthMatchingAlg ::
  initialize ()
  {
    ANA_CHECK (m_matchingTool.retrieve());
    ANA_CHECK (m_tauHandle.initialize (m_systematicsList));
    ANA_CHECK (m_preselection.initialize (m_systematicsList, m_tauHandle, SG::AllowEmpty));
    ANA_CHECK (m_systematicsList.initialize());
    return StatusCode::SUCCESS;
  }



  StatusCode TauTruthMatchingAlg ::
  execute ()
  {
    for (const auto& sys : m_systematicsList.systematicsVector())
    {
      const xAOD::TauJetContainer *taus = nullptr;
      ANA_CHECK (m_tauHandle.retrieve (taus, sys));
      for (const xAOD::TauJet *tau : *taus)
      {
        if (m_preselection.getBool (*tau, sys))
        {
          m_matchingTool->getTruth (*tau);
        }
      }
      ANA_CHECK( m_matchingTool->lockDecorations (*taus) );
    }
    return StatusCode::SUCCESS;
  }
}
