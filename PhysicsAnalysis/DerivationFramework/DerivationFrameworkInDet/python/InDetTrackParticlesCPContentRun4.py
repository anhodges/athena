# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration

InDetTrackParticlesCPContentRun4 = [
"InDetTrackParticles",
"InDetTrackParticlesAux.time.timeResolution.hasValidTime"
]
