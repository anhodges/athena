#include "../../JetToolHelpers/HistoInput1D.h"
#include "../../JetToolHelpers/HistoInput2D.h"
#include "../../JetToolHelpers/VarTool.h"
#include "../../JetToolHelpers/TextInputMCJES.h"

DECLARE_COMPONENT( JetHelper::HistoInput1D )
DECLARE_COMPONENT( JetHelper::HistoInput2D )
DECLARE_COMPONENT( JetHelper::VarTool )
DECLARE_COMPONENT( JetHelper::TextInputMCJES )
