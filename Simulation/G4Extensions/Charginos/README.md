# Charginos
Author Andrea Dell'Acqua (dellacqu@mail.cern.ch)
Converted from packagedoc.h

## Introduction

This package introduces charginos (charged SUSY particles) into the Geant4 simulation.  
The properties of the charginos are not set in this package, since they can vary depending on 
the region of parameter space one wishes to consider.

## Class Overview

This package contains only one class:

 - CharginosProcessDefinition : Responsible for assigning physics processes to the Charginos, such as transportation.
