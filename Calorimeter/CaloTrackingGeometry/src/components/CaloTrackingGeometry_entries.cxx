#include "CaloTrackingGeometry/CaloTrackingGeometryBuilder.h"
#include "../CaloSurfaceBuilder.h"
#include "CaloTrackingGeometry/CaloTrackingGeometryBuilderCond.h"

using namespace Calo;

DECLARE_COMPONENT( CaloTrackingGeometryBuilder )
DECLARE_COMPONENT( CaloSurfaceBuilder )
DECLARE_COMPONENT( CaloTrackingGeometryBuilderCond )
