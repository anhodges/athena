#include "../InDetAlignDBTool.h"
#include "../InDetAlignTrackSelTool.h"
#include "../InDetAlignFillTrack.h"

#include "../InDetAlignOverlapTool.h"
#include "../InDetAlignFillSiCluster.h"
#include "../RefitSiOnlyTool.h"
#include "../InDetAlignHitQualSelTool.h"


DECLARE_COMPONENT( InDetAlignDBTool )
DECLARE_COMPONENT( InDetAlignTrackSelTool )
DECLARE_COMPONENT( InDetAlignFillTrack )

DECLARE_COMPONENT( InDetAlignOverlapTool )
DECLARE_COMPONENT( InDetAlignFillSiCluster )
DECLARE_COMPONENT( InDetAlignHitQualSelTool )
DECLARE_COMPONENT( InDetAlignment::RefitSiOnlyTool )

