/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#include "../TruthOverlay.h"
DECLARE_COMPONENT(MuonR4::TruthOverlay)